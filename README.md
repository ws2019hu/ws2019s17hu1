# WorldSkills 2017 Nemzeti Verseny, Skill 17 (Web Design and Development)


## 1. forduló

Az alábbiakban a válogató első fordulójára vonatkozó legfontosabb információkat osztjuk meg.
### Általános tudnivalók
Ebben a fordulóban egy JavaScript alapú játékot kell elkészítened. Az otthoni feladat megoldása során tetszőleges forrásokat használhatsz fel, de a feladatot önállóan kell elvégezned, az elkészült munkának a te képességeidet és tudásodat kell tükröznie.

A beküldött feladat értékelését követően a legjobb 10-12 versenyzővel személyes interjú keretében beszélgetünk el. Ezt követően 6 versenyzőt választunk ki, ők mérkőzhetnek majd meg egymással az egynapos középdöntőben.

#### A feladat beadásának módja és határideje:

A feladatok beadásához a megoldást tartalmazó BitBucket repository-t (a továbbiakban repo) kell majd megosztanod velünk. YY az a kétszámjegyű személyes kódod, amit e-mailben küldtünk ki számodra. Amennyiben nem használtad még a BitBucket szolgáltatást, akkor regisztrálj egy azonosítót a https://bitbucket.org címen, majd hozz létre egy privát repo-t YY_WS2019HUS17_1 néven, ahol YY a tőlünk e-mailben kapott kétszámjegyű sorszámod. 

Ha nem használtál még git-et egyáltalán, akkor legfőbb ideje, hogy megismerkedj vele. Nem egy ördöglakat, és most nem is lesz még szükséged rá, hogy nagyon elmélyülj benne. Elég, ha alapszinten megérted a működését, és az alapfunkciók használatával az ekészült munkádat feltöltöd a létrehozott repo-ba.

Ezután már csak egy feladatod lesz: a megoldásodat tartalmazó repo-t meg kell osztanod velünk. Ehhez a ws2019@netskills.hu címünket használd. Elegendő, ha READ jogosultságot kapunk.

Ha segítségre van szükséged a fentiekhez, akkor írj nyugodtan nekünk a fenti címre. 

A feladatok elkészítésének végső határideje: **február 21., szerda éjfél**

Beadott mgoldásnak azt tekintjük, amit a fenti időben a repodban találunk.  

#### A feladat

A feladat leírását és a megoldáshoz szükséges vagy opcionálsan felhasználható média fájlokat ebben a repoban taalálod meg. Ha mindezt szeretnéd a saját gépedre varázsolni, akkor máris használhatod az egyik alap git parancsot, a clone-t. Ha még nincs git a gépeden, telepítsd fel (https://git-scm.com/), majd egy megfelelő mappában add ki a git clone https://bitbucket.org/ws2019hu/ws2017s17hu1 parancsot (profik és bevállalosabbak persze ehhez már valamilyen grafikus eszközt fognak használni, pl. a SourceTree-t).

Érdemes időről-időre visszatérni ide, vagy frissíteni a lokális változatot (git pull), mert lehetnek változások.

A feladat leírását a first-roud mappában fellelhető WSC2019_NATIONAL_HU_S17_1.pdf fájlban található. A kiegészítő média fájlok a WSC2019_NATIONAL_HU_S17_1_MEDIA.zip archívumban vannnak.    
